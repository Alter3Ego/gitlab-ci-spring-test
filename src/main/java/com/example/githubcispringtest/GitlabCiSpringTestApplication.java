package com.example.githubcispringtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabCiSpringTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabCiSpringTestApplication.class, args);
    }

}
